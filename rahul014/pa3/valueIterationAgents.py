# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

        # Write value iteration code here
        "*** YOUR CODE HERE ***"

        # Store all transition probabilities for all states from the given state
        # in a dict
        self.transitions = dict()
        self.rewards = dict()
        self.nextstates = dict()
        
        for states in self.mdp.getStates():

          # If its a terminal state ignore
          if not self.mdp.isTerminal(states):
            possibleActions = self.mdp.getPossibleActions(states)
            
            # Fill up the dictionaries.
            for action in possibleActions:
              # For every action get the transition states and the probabilities associated.
              for (ts , p) in self.mdp.getTransitionStatesAndProbs(states,action):
                
                #If state,action pair is present in the nextstates dictionary
                if (states, action) not in self.nextstates:
                  self.nextstates[(states, action)] = [ts]
                else:
                  self.nextstates[(states, action)] += [ts]

                # Fill up the transition and rewards dictionaries.  
                self.transitions[(states, action, ts)] = p
                self.rewards[(states,action,ts)] = self.mdp.getReward(states, action,ts)

        # Value Iteration Code

        for iter in range(iterations):
          temp = util.Counter()

          for allStates in self.mdp.getStates():
            if not self.mdp.isTerminal(allStates):
              actions = self.mdp.getPossibleActions(allStates)

              # Catch all the optimal values here 
              optimalValues = []
              for a in actions:
                optimalValues += [self.getQValue(allStates, a)]

              # Select the best one 
              temp[allStates] = max(optimalValues)
          self.values = temp.copy();

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        # Using bellman eqns here to compute Q-values
        sum = 0;
        for ts in self.nextstates[state, action]:
          val1 = self.rewards[state,action,ts] + self.discount * self.values[ts]
          val = self.transitions[state, action, ts]*val1
          sum += val

        return sum

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        
        # Check if its a terminal state
        res = self.mdp.isTerminal(state)

        if not res:
          # Return maximum of the Q Values
          allQValues = []
          for a in self.mdp.getPossibleActions(state):
             qvalue = self.getQValue(state,a)
             allQValues += [(qvalue,a)]
          return max(allQValues)[1]   # [1] denoting the action returned
        
        return None


    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
