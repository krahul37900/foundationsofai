# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood().asList()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
      
        INF = 1000000
        if action == Directions.STOP:
          return -INF

        oldFood = currentGameState.getFood().asList()

        # Check if the ghost eats pacman
        for ghost in successorGameState.getGhostPositions():
          if newPos == ghost and ghostState.scaredTimer == 0:
            return -INF

        # Manhattan Distance between pacman and the closest food.
        dist_pac_food = []
        for eachfood in oldFood:
          dist_pac_food.append( util.manhattanDistance(newPos, eachfood)) 

        return INF - min(dist_pac_food)

        

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """
    
    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
      
        
        #print gameState.getLegalActions(0)
        # Return an action with the highest utility value after running the minimax
        # algorithm.
        actions = gameState.getLegalActions(self.index)
        optimalAction = ""
        optimalValue = -(float("inf"))
        numGhosts = gameState.getNumAgents() - 1
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            utilValue = self.min_value(nextState, self.depth, 1, numGhosts)

            if utilValue > optimalValue:
               optimalValue = utilValue
               optimalAction = action
        return optimalAction

    def min_value(self,gameState,depth,nGhost,numGhosts):
      
        # terminal state
        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        v = float("inf")
        if nGhost == numGhosts: #Last Ghost
           actions = gameState.getLegalActions(nGhost)
           for action in actions:
               nextState = gameState.generateSuccessor(nGhost, action)
               v = min(v, self.max_value(nextState, depth - 1, numGhosts))         
               # decrease depth here only.
        else:
            actions = gameState.getLegalActions(nGhost)
            for action in actions:
                nextState = gameState.generateSuccessor(nGhost, action)
                v = min(v, self.min_value(nextState, depth, nGhost + 1, numGhosts))    
        return v

        
    def max_value(self,gameState,depth,numGhosts):

        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        v = -(float("inf"))
        actions = gameState.getLegalActions(0)
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            v = max(v, self.min_value(nextState, depth , 1,numGhosts))
        return v


  
class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
             
        actions = gameState.getLegalActions(self.index)
        optimalAction = ""
        optimalValue = -(float("inf"))
        alpha = -(float("inf"))
        beta = float("inf")
        numGhosts = gameState.getNumAgents() - 1
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            utilValue = self.min_value(nextState, self.depth, 1, numGhosts,alpha, beta)

            if utilValue > optimalValue:
               optimalValue = utilValue
               optimalAction = action
            if utilValue > beta:
               return optimalAction
            alpha = max(alpha, optimalValue)   
        return optimalAction

    def min_value(self,gameState,depth,nGhost,numGhosts,alpha,beta):
      
        # terminal state
        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        v = float("inf")
        if nGhost == numGhosts: #Last Ghost
           actions = gameState.getLegalActions(nGhost)
           for action in actions:
               nextState = gameState.generateSuccessor(nGhost, action)
               v = min(v, self.max_value(nextState, depth - 1, numGhosts,alpha,beta)) 
               if v < alpha:
                  return v
               beta = min(beta,v)         
               # decrease depth here only.
        else:
            actions = gameState.getLegalActions(nGhost)
            for action in actions:
                nextState = gameState.generateSuccessor(nGhost, action)
                v = min(v, self.min_value(nextState, depth, nGhost + 1, numGhosts,alpha,beta))  
                if v < alpha:
                   return v
                beta = min(beta,v)   
     
        return v

        
    def max_value(self,gameState,depth,numGhosts,alpha,beta):

        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        v = -(float("inf"))
        actions = gameState.getLegalActions(0)
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            v = max(v, self.min_value(nextState, depth , 1,numGhosts,alpha,beta))
            if v > beta:
               return v
            alpha = max(alpha,v)
        return v



class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"

                #print gameState.getLegalActions(0)
        # Return an action with the highest utility value after running the minimax
        # algorithm.
        actions = gameState.getLegalActions(self.index)
        optimalAction = ""
        optimalValue = -(float("inf"))
        numGhosts = gameState.getNumAgents() - 1
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            utilValue = self.min_value(nextState, self.depth, 1, numGhosts)

            if utilValue > optimalValue:
               optimalValue = utilValue
               optimalAction = action
        return optimalAction

    def min_value(self,gameState,depth,nGhost,numGhosts):
      
        # terminal state
        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        #v = float("inf")
        avg = 0
        if nGhost == numGhosts: #Last Ghost
           actions = gameState.getLegalActions(nGhost)
           len1 = len(actions)
           for action in actions:
               nextState = gameState.generateSuccessor(nGhost, action)
               #v = min(v, self.max_value(nextState, depth - 1, numGhosts))         
               avg += self.max_value(nextState, depth - 1, numGhosts)
           avg = float(avg)/len1 
               # decrease depth here only.
        else:
            actions = gameState.getLegalActions(nGhost)
            len2 = len(actions)
            for action in actions:
                nextState = gameState.generateSuccessor(nGhost, action)
                avg += self.min_value(nextState, depth, nGhost + 1, numGhosts) 
            avg = float(avg)/len2
        return avg

        
    def max_value(self,gameState,depth,numGhosts):

        if gameState.isWin() or gameState.isLose() or depth == 0:
           return self.evaluationFunction(gameState)

        v = -(float("inf"))
        actions = gameState.getLegalActions(0)
        for action in actions:
            nextState = gameState.generateSuccessor(0, action)
            v = max(v, self.min_value(nextState, depth , 1,numGhosts))
        return v
        
       

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
      
      I return positive infinity for winnning states and negetive inf for 
      loosing states.

      Calculate the nearest food pallet from the pacman and assign it a high value.
      If any ghost is within 5 units of distance from pacman , the pacman tries to take 
      states where it goes further away from the ghost, if it is a scared ghost the pacman and 
      within that distance I try to eat it within a relatively short time.


    """
    "*** YOUR CODE HERE ***"

    # Final utility value for the state.
    result = currentGameState.getScore(); 
    curPos = currentGameState.getPacmanPosition()

    # Best state
    if currentGameState.isWin():
       return float("inf")
    # Worst state   
    if currentGameState.isLose():
       return -(float("inf"))
    
    # Find the closest food.
    newFood = currentGameState.getFood().asList()
    nearestFood = float("inf")
    for food in newFood:
        dist = util.manhattanDistance(curPos, food)
        if dist < nearestFood:
           nearestFood = dist

    # Find the closest ghost.
    ghosts = currentGameState.getGhostStates()
    nearestGhost = float("inf")
    scaredtime = float("inf")
    count = 1;
    for ghost in ghosts:
        dist = util.manhattanDistance(curPos, currentGameState.getGhostPosition(count))
        if dist < nearestGhost:
           nearestGhost = dist
           scaredtimer = ghost.scaredTimer
        count += 1 
    # Farther the ghost , the better it is.
    if scaredtimer >= nearestGhost:
       result -= nearestGhost*2
    else:
        result += max(nearestGhost,4)*2 
    # Closer the food , the better it is.
    result -= nearestFood*1.5
    # Closer the pallet , the better it is.
    capsules = len(currentGameState.getCapsules())
    result -= capsules*3.5

    # Have to include remaining food also as a variable in determining utility value
    result -= len(newFood)*4
    
    return result








# Abbreviation
better = betterEvaluationFunction

